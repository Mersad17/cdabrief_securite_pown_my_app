# login/urls.py
from django.urls import path
from .views import crsf_exemple, homePageView, welcomeUser,xss_exemple

urlpatterns = [
    path("", homePageView, name="home"),
    path("welcomeUser/<str:name>", welcomeUser, name="welcomeuser"),
    path("crsf_exempe", crsf_exemple, name= "crsf_exemple"),
    path("xss/", xss_exemple, name= "xss"),

] 