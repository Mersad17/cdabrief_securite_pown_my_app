from ast import Param
from audioop import reverse
from imaplib import _Authenticator
from multiprocessing import AuthenticationError
from urllib import request
from django.http import HttpResponse
from django.shortcuts import  render, redirect
from .forms import NameForm
from django.contrib.auth import login, authenticate, logout #add this
from django.contrib import messages
from django.contrib.auth import login, authenticate 
from django.contrib.auth.forms import AuthenticationForm 
from django.contrib.auth.models import User



# Create your views here.
def homePageView(request):
    return HttpResponse("Hello, World!")

def welcomeUser(request,name):
    return HttpResponse("Welcome "+name)

def crsf_exemple(request):
    # if this is a POST request we need to process the form data
	data = request.POST
    	
	return render(request, 'crsf_exemple.html', {'data':data})

def xss_exemple(request):

    # user = User.objects.order_by('id')
    # if request.GET.get('name'):
	#       User(name=request.GET.get('name')).save()
    return render(request, 'xss.html', {'data':'user'})