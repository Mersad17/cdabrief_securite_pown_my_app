from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import CharField, Form, PasswordInput

# Create your forms here.
class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)
    password = forms.CharField(widget=PasswordInput())
